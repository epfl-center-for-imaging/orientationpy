.. complexity documentation master file, created by
   sphinx-quickstart on Tue Jul  9 22:26:36 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. .. include:: ../README.rst

Contents:
=========

.. toctree::
   :maxdepth: 2
   :caption: OrientationPy

   introduction
   installation
   conventions
   authors

.. toctree::
   :maxdepth: 2
   :caption: Examples Gallery

   orientationpy_examples/index

.. toctree::
   :maxdepth: 2
   :caption: Module Documentation

   modules

..    usage
..    contributing
..    history.rst


..


Feedback
========
