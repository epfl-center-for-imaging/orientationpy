Todo
=========

 - [X] 3D tests

 - [X] 3D coherency and energy

 - [X] integrate gradients and structure tensor into gradientStructureTensor

 - [X] Change 3D orientation system to z-top

 - [X] compare (and check compatibility) to skimage structure tensor

 - [X] write anglesToVectors()

 - [X] numba speedup?

 - [X] dict output to computeOrientations

 - [X] Write sphinx home page

 - [x] spam plots

 - [X] factor for non-isotropic voxels

 - [_] in Boxed functions, return box centres? AND/OR accept centres and sizes?

 - [_] VTK out

 - [_] scripts with tiff in?

 - [_] rose plot

 - [_] rename plotOrientations → ZalignedStereoPlot?

 - [_] investigate smoothed 'pole plots'

 - [_] in `plotting.py` create an image synthesizer?

 - [_] mask greylevels
