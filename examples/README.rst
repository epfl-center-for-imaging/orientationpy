.. _orientationpyExamples:

OrientationPy examples
-----------------------------

These small jupyter notebooks (look at the bottom of each one to download `*.ipynb` file!) illustrate how to use OrientationPy on some small examples.
