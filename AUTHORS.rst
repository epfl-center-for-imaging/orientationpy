========
Authors
========

 * Initial prototype: Alexandru Vasile <vasilealexandrupetru@gmail.com>
 * Main development: Edward Andò <edward.ando@epfl.ch>
 * Development: Florian Aymanns <florian.aymanns@epfl.ch>
 * Development: Mallory Wittwer <mallory.wittwer@epfl.ch>
 * Scientific support: Daniel Sage <daniel.sage@epfl.ch>

 * Stereo plot: Max Wiebicke <max.wiebicke@sydney.edu.au>
 * Spherical Histogram and analyse/generate: Gustavo Pinzon  <gustavo.pinzon@3sr-grenoble.fr>
